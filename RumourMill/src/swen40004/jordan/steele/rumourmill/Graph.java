package swen40004.jordan.steele.rumourmill;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * A representation of an undirected graph using an adjacency list
 * @author jordansteele
 *
 * @param <T> The type of the nodes
 */
public class Graph<T> {
	
	//The actual adjacency list implemented as a hash map from the type to
	//an array list of the type
	private HashMap<T, ArrayList<T>> adjacencyList; 
	
	/**
	 * Constructor
	 */
	public Graph() {
		adjacencyList = new HashMap<T, ArrayList<T>>();
	}
	
	/**
	 * Adds a link between two vertices
	 * @param vertex1 The first vertex
	 * @param vertex2 The second vertex
	 */
	public void addRelationship(T vertex1, T vertex2) {
		getNeighbours(vertex1).add(vertex2);
		getNeighbours(vertex2).add(vertex1);
	}
	
	/**
	 * Gets the neighbours of a vertex
	 * @param vertex The vertex to find the neighbours of
	 * @return A list of the neighbours
	 */
	public ArrayList<T> getNeighbours(T vertex) {
		ArrayList<T> vertexNeighbours = adjacencyList.get(vertex);
		if (vertexNeighbours == null) {
			adjacencyList.put(vertex, new ArrayList<T>());
			vertexNeighbours = adjacencyList.get(vertex);
		}
		return vertexNeighbours;
	}
}
