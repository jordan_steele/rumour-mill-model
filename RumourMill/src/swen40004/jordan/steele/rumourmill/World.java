package swen40004.jordan.steele.rumourmill;

import java.util.ArrayList;
import java.util.Collections;

/**
 * A representation of the 'world' implemented as a 2D array storing 'cells'
 * @author jordansteele
 *
 */

public class World {
	
	private Cell[][] world; //the actual 'world' 2D array
	private int clique;		//the size of the clique in the world
	private Coord[] rumourSources; //the rumour sources in the world
	
	/**
	 * Constructor
	 * @param xdimen Size of the world in the x dimension
	 * @param ydimen Size of the world in the y dimension
	 * @param rumourSources Coordinates of the rumour sources
	 * @param rumourAbout Coordinate of who the rumour is about, can be null
	 */
	public World(int xdimen, int ydimen, Coord[] rumourSources, 
			Coord rumourAbout)
	{
		this.clique = 0;
		this.rumourSources = rumourSources;
		this.world = initWorld(xdimen, ydimen, rumourSources, rumourAbout);
	}
	
	/**
	 * Constructor
	 * @param xdimen Size of the world in the x dimension
	 * @param ydimen Size of the world in the y dimension
	 * @param intialClique Fraction of people who initially know the rumour
	 * @param rumourAbout Coordinate of who the rumour is about, can be null
	 */
	public World(int xdimen, int ydimen, float initialClique, 
			Coord rumourAbout)
	{
		this(xdimen, ydimen, 
				genInitRumourSources(xdimen, ydimen, initialClique), 
				rumourAbout);
	}
	
	/**
	 * Getter for clique
	 * @return The value for clique
	 */
	public int getClique() {
		return clique;
	}
	
	/**
	 * Gets the cell at the specified location
	 * @param x Location in the x dimension
	 * @param y Location in the y dimension
	 * @return The cell at the specified location
	 */
	public Cell getCell(int x, int y) {
		return world[x][y];
	}
	
	/**
	 * Calculates the amount of cells in the world
	 * @return The amount of cells in the world
	 */
	public int size() {
		return this.world.length * this.world[0].length;
	}
	
	/**
	 * Calculates the size of the world in the x dimension
	 * @return The size of the world in the x dimension
	 */
	public int sizeX() {
		return this.world.length;
	}
	
	/**
	 * Calculates the size of the world in the y dimension
	 * @return The size of the world in the y dimension
	 */
	public int sizeY() {
		return this.world[0].length;
	}
	
	/**
	 * Updates the world by handling the people who have just heard the rumour
	 * @param ticks The current ticks the simulation is at
	 */
	public void update(int ticks) {
		//For each person
		for (int i=0; i < sizeX(); i++) {
			for (int j=0; j < sizeY(); j++) {
				if (getCell(i, j) instanceof Person) {
					Person currentPerson = (Person) getCell(i, j);
					
					//If they just heard the rumour
					if (currentPerson.getJustHeardFrom() != null) {
						//handle hearing the rumour
						if (currentPerson.getTimesHeard() == 0) {
							clique++;
							currentPerson.setFirstTimeHeard(ticks);
							currentPerson.setKnowledgeOfRumour(
									currentPerson.getJustHeardFrom().
									getKnowledgeOfRumour() 
									* 0.98f);
						}
						currentPerson.setJustHeardFrom(null);
						currentPerson.incrementTimesHeard();
					}
				}
			}
		}
	}
	
	/**
	 * Resets an already initialised world to how it was when initially
	 * initialised
	 */
	public void resetWorld() {
		//Set the clique to 0
		this.clique = 0;
		
		//For each person
		for (int i=0; i < world.length; i++) {
			worldcol:for (int j=0; j < world[0].length; j++) {
				if (getCell(i, j) instanceof Person) {
					
					//Reset the variables changed during a simulation
					Person currentPerson = (Person) getCell(i, j);
					
					for(Coord rumourSource : rumourSources) {
						if (i == rumourSource.x && j == rumourSource.y) {
							currentPerson.setTimesHeard(1);
							currentPerson.setFirstTimeHeard(0);
							currentPerson.setKnowledgeOfRumour(1.0f);
							this.clique++;
							continue worldcol;
						}
					}
					currentPerson.setTimesHeard(0);
					currentPerson.setFirstTimeHeard(-1);
					currentPerson.setKnowledgeOfRumour(0.0f);
				}
			}
		}
	}
	
	/**
	 * Initialise a world
	 * @param xdimen The size of the world in the x dimension
	 * @param ydimen The size of the world in the y dimension
	 * @param rumourSources The coordinates of those who know the rumour at t=0
	 * @param rumourAbout The coordinate of who the rumour is about
	 * @return The initialised world
	 */
	private Cell[][] initWorld(int xdimen, int ydimen, Coord[] rumourSources, 
			Coord rumourAbout) {
		Cell[][] world = new Cell[xdimen][ydimen];
		double maximumDistanceBetweenCells = Math.sqrt(Math.pow(xdimen, 2) +
				Math.pow(ydimen, 2));
		
		for (int i=0; i < world.length; i++) {
			for (int j=0; j < world[0].length; j++) {
				//Construct the new person object
				Person newPerson = new Person();
				
				//if current coords a rumourSource set the newPerson to having
				//heard the rumour at time 0
				for(Coord rumourSource : rumourSources) {
					if (i == rumourSource.x && j == rumourSource.y) {
						newPerson.setFirstTimeHeard(0);
						newPerson.incrementTimesHeard();
						newPerson.setKnowledgeOfRumour(1.0f);
						this.clique++;
					}
				}
				
				//Change the change to pass on the rumour if the rumour is 
				//about someone, from one to a value proportional to the 
				//distance to who the rumour is about
				if (rumourAbout != null) {
					rumourAbout.distanceTo(new Coord(i, j));
					newPerson.setChanceToPassOnRumour((float) (1 - 
							(rumourAbout.distanceTo(new Coord(i, j)) / 
									maximumDistanceBetweenCells) * 0.7));
				}
				
				//add the new person to the array
				world[i][j] = newPerson;
			}
		}
		
		return world;
	}
	
	/**
	 * Creates an array where the initial rumour sources should be located based
	 * on the dimensions of the world and the fraction of people who should
	 * initially know the rumour
	 * @param xdimen
	 * @param ydimen
	 * @param initialClique
	 * @return
	 */
	private static Coord[] genInitRumourSources(int xdimen, int ydimen, 
			float initialClique) {
		ArrayList<Coord> potentialSources = new ArrayList<Coord>();
		for (int x=0; x < xdimen; x++) {
			for (int y=0; y < ydimen; y++) {
				potentialSources.add(new Coord(x, y));
			}
		}
		int initCliqueSize = (int)(potentialSources.size() * initialClique);
		Collections.shuffle(potentialSources);
		return potentialSources.subList(0, initCliqueSize).
				toArray(new Coord[initCliqueSize]);
	}
	
}
