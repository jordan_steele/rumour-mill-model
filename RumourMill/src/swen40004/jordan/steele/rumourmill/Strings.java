package swen40004.jordan.steele.rumourmill;

/**
 * This class contains all the user facing strings in the application as static
 * variables.
 * @author jordansteele
 *
 */

public class Strings {

	//Main.java
	public static final String USAGE_ERROR = "Usage: java -cp <classpath> " +
			"<class> <iterations> <wrapAround> <xDimen> <yDimen> " +
			"<neighbourhood> <[(rumourSourceX,rumourSourceY), ...] " +
			"| initClique> <[(wallX,wallY), ...]> <rediscussRumour> " +
			"<dataOutputFolder> <optional(rumourAboutX,rumourAboutY)>\n";
	public static final String PARSE_INT_ERROR = "Argument %s must be an " +
			"integer.\n";
	public static final String PARSE_NEIGHBOUR_ERROR = "Argument %s must be " +
			"either 0 (von Neumann) or 1 (Moore).\n";
	public static final String PARSE_BOOL_ERROR = "Argument %s must be " +
			"'True' or 'False'.\n";
	public static final String PARSE_COORDS_ERROR = "Argument %s must follow " +
			"the format [(x, y), (x2, y2) ... ].\n";
	public static final String NO_RUMOUR_ABOUT_MESS = "No argument provided " +
			"for who the rumour is about -> setting the chance of passing " +
			"on the rumour to 1 for all people.\n";
	
	//RumourMill.java
	public static final String COMPLETED_RUN = "Completed run %d.\n";
	public static final String RUNNING_DATA_FILENAME = "runningData.csv";
	public static final String FINAL_PERSON_DATA_FILENAME = "endData.csv";
}
