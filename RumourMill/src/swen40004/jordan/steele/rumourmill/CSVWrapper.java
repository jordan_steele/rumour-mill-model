package swen40004.jordan.steele.rumourmill;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import au.com.bytecode.opencsv.CSVWriter;

/**
 * A simple wrapper to make opening and closing a csv cleaner
 * @author jordansteele
 *
 */
public class CSVWrapper {

	/**
	 * Creates the directories required and open/create the file as a CSVWriter
	 * @param filepath The filepath of the file to open/create as a string
	 * @return The CSVWriter to write to the csv
	 */
	public static CSVWriter openCSV(String filepath) {
		CSVWriter writer = null;
		//Open the csv file
		try {
			File file = new File(filepath);
			file.getParentFile().mkdirs();
			writer = new CSVWriter(new FileWriter(file), ',', 
					CSVWriter.NO_QUOTE_CHARACTER);		
		} catch (IOException e) {
			System.err.println("Failed to open csv file.");
			System.exit(1);
		}
		return writer;
	}
	
	/**
	 * Closes the CSVWriter
	 * @param writer The CSVWriter to close
	 */
	public static void closeCSV(CSVWriter writer) {
		//Close the csv file
		try {
			writer.close();
		} catch (IOException e) {
			System.err.println("Failed to close csv file.");
			System.exit(1);
		}
	}
	
}
