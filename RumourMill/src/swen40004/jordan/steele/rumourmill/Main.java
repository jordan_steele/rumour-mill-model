package swen40004.jordan.steele.rumourmill;

import java.util.ArrayList;

/**
 * This is the main class that parses the command line arguments and then 
 * creates the rumour mill model and runs it.
 * @author jordansteele
 *
 */

public class Main {

	/**
	 * Main method
	 * @param args
	 */
	public static void main(String[] args) {
		if (args.length >= 8) {
			//parse args
			int iterations = parseIntArgument(args[0]);
			boolean wrapAround = parseBoolArgument(args[1]);
			int xDimen = parseIntArgument(args[2]);
			int yDimen = parseIntArgument(args[3]);
			RumourMill.Neighbourhood neighbourhood = 
					parseNeighbourhood(args[4]);
			Coord[] walls = parseCoordsArgument(args[6]);
			boolean rediscussRumour = parseBoolArgument(args[7]);
			String outputFolder = args[8];
			Coord rumourAbout = null;
			if (args.length == 10) {
				rumourAbout = parseCoordArgument(args[9]);
			}
			
			float initialClique = parsePercentageArgument(args[5]);
			RumourMill rumourMill;
			if (initialClique == -1) {
				Coord[] rumourSources = parseCoordsArgument(args[5]);
				rumourMill = new RumourMill(wrapAround, xDimen, yDimen, 
						rumourSources, walls, neighbourhood, rumourAbout, 
						rediscussRumour, outputFolder);
			} 
			else {
				rumourMill = new RumourMill(wrapAround, xDimen, yDimen, 
						initialClique, walls, neighbourhood, rumourAbout, 
						rediscussRumour, outputFolder);
			}
			
			rumourMill.runModel(iterations);
		}
		else {
			System.err.println(Strings.USAGE_ERROR);
			System.exit(1);
		}
	}
	
	/**
	 * Takes a string argument and returns an Integer if possible, otherwise
	 * throws an error and ends the program if the string cannot be passed into 
	 * an Integer.
	 * @param arg The argument to parse
	 * @return The argument parsed into it's Integer representation or -1
	 */
	private static int parseIntArgument(String arg) {
		try {
			return Integer.parseInt(arg);
		} catch (NumberFormatException e) {
			System.err.printf(Strings.PARSE_INT_ERROR, arg);
	        System.exit(1);
	        return -1;
		}
	}
	
	/**
	 * Takes a string argument and returns a neighbourhood method if possible, 
	 * otherwise throws an error and ends the program if the string cannot be 
	 * passed into a neighbourhood method.
	 * @param arg The argument to parse
	 * @return The argument parsed into it's neighbourhood representation
	 */
	private static RumourMill.Neighbourhood parseNeighbourhood(String arg) {
		try {
			return RumourMill.Neighbourhood.values()[Integer.parseInt(arg)];
		} catch (Exception e) {
			System.err.printf(Strings.PARSE_NEIGHBOUR_ERROR, arg);
	        System.exit(1);
	        return RumourMill.Neighbourhood.MOORE;
		}
	}
	
	/**
	 * Takes a string argument and returns a boolean if possible, otherwise
	 * throws an error and ends the program if the string cannot be passed into 
	 * a boolean value.
	 * @param arg The argument to parse
	 * @return The argument parsed into it's boolean representation
	 */
	private static boolean parseBoolArgument(String arg) {
		try {
			return Boolean.parseBoolean(arg);
		} catch (NumberFormatException e) {
			System.err.printf(Strings.PARSE_BOOL_ERROR, arg);
	        System.exit(1);
	        return false;
		}
	}
	
	/**
	 * Takes a string argument and returns an array of Coords if possible, 
	 * otherwise throws an error and ends the program if the string cannot be 
	 * passed into an array of Coords.
	 * @param arg The argument to parse
	 * @return The argument parsed into an array of Coords
	 */
	private static Coord[] parseCoordsArgument(String arg) {
		ArrayList<Coord> coords = new ArrayList<Coord>();
		
		//Create a simple array of all the integers in the input
		arg = arg.replace("(", "");
		arg = arg.replace(")", "");
		arg = arg.replace("[", "");
		arg = arg.replace("]", "");
		arg = arg.replace(" ", "");
		String[] values = arg.split(",");
		
		//Take two integers (still in string form) at a time and create a coord
		//and add the coord to our coord array
		for (int i = 0, j = 1; j < values.length; i += 2, j += 2) {
			try {
				coords.add(new Coord(Integer.parseInt(values[i]), 
						Integer.parseInt(values[j])));
			} catch (Exception e) {
				System.err.printf(Strings.PARSE_COORDS_ERROR, arg);
		        System.exit(1);
			}
		}
		
		return coords.toArray(new Coord[coords.size()]);
	}
	
	/**
	 * Takes a string argument and returns a float between 0 and 1, otherwise
	 * returns -1
	 * @param arg The argument to parse
	 * @return A float between 0 and 1, otherwise -1
	 */
	private static float parsePercentageArgument(String arg) {
		try {
			return Integer.parseInt(arg) / 100.0f;
		} catch (NumberFormatException e) {
	        return -1;
		}
	}
	
	/**
	 * Takes a string argument and returns a Coord if possible, otherwise
	 * throws an error and ends the program if the string cannot be passed into 
	 * a Coord.
	 * @param arg The argument to parse
	 * @return The argument parsed into it's Coord representation
	 */
	private static Coord parseCoordArgument(String arg) {
		try {
			return parseCoordsArgument(arg)[0];
		} catch (IndexOutOfBoundsException e) {
			System.out.println(Strings.NO_RUMOUR_ABOUT_MESS);
			return null;
		}
	}

}
