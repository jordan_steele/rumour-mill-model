package swen40004.jordan.steele.rumourmill;

/**
 * Represents a Person in the RumourMill world
 * @author jordansteele
 *
 */
public class Person extends Cell {

	private int timesHeard;	//how many times the person has heard the rumour
	private int firstTimeHeard; //the time the person first heard the rumour
	private Person justHeardFrom; //set a person if that person just told this 
									//person the rumour
	private float chanceToPassOnRumour; //value between 0 and 1
	private float knowledgeOfRumour; //value between 0 and 1
	
	/**
	 * Constructor
	 * Sets initial values for all instance variables
	 */
	public Person() {
		this.timesHeard = 0;
		this.firstTimeHeard = -1;
		this.justHeardFrom = null;
		this.chanceToPassOnRumour = 1.0f;
		this.knowledgeOfRumour = 0.0f;
	}

	/**
	 * Getter for timesHeard
	 * @return timesHeard
	 */
	public int getTimesHeard() {
		return timesHeard;
	}

	/**
	 * Setter for timesHeard
	 * @param timesHeard The value to set timesHeard to
	 */
	public void setTimesHeard(int timesHeard) {
		this.timesHeard = timesHeard;
	}
	
	/**
	 * Increments timesHeard by 1
	 */
	public void incrementTimesHeard() {
		this.timesHeard++;
	}

	/**
	 * Getter for firstTimeHeard
	 * @return firstTimeHeard instance variable
	 */
	public int getFirstTimeHeard() {
		return firstTimeHeard;
	}

	/**
	 * Setter for firstTimeHeard
	 * @param firstTimeHeard The value to set firstTimeHeard to
	 */
	public void setFirstTimeHeard(int firstTimeHeard) {
		this.firstTimeHeard = firstTimeHeard;
	}

	/**
	 * Getter for justHeardFrom
	 * @return justHeardFrom value
	 */
	public Person getJustHeardFrom() {
		return justHeardFrom;
	}

	/**
	 * Setter for justHeardFrom
	 * @param justHeardFrom The value to set justHeardFrom to
	 */
	public void setJustHeardFrom(Person justHeardFrom) {
		this.justHeardFrom = justHeardFrom;
	}

	/**
	 * Getter for chanceToPassOnRumour
	 * @return The value for chanceToPassOnRumour
	 */
	public float getChanceToPassOnRumour() {
		return chanceToPassOnRumour;
	}

	/**
	 * Setter for chanceToPassOnRumour
	 * @param chanceToPassOnRumour The value to set chanceToPassOnRumour to
	 */
	public void setChanceToPassOnRumour(float chanceToPassOnRumour) {
		this.chanceToPassOnRumour = chanceToPassOnRumour;
	}

	/**
	 * Getter for knowledgeOfRumour
	 * @return The value of knowledgeOfRumour
	 */
	public float getKnowledgeOfRumour() {
		return knowledgeOfRumour;
	}

	/**
	 * Setter for knowledgeOfRumour
	 * @param knowledgeOfRumour The value to set knowledgeOfRumour to
	 */
	public void setKnowledgeOfRumour(float knowledgeOfRumour) {
		this.knowledgeOfRumour = knowledgeOfRumour;
	}
	
}
