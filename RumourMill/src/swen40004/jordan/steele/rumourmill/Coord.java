package swen40004.jordan.steele.rumourmill;

/**
 * Essentially a tuple to represent a coordinate in two dimensions
 * @author jordansteele
 *
 */
public class Coord {
	public final int x; //position in x dimension
	public final int y; //position in y dimension
	
	/**
	 * Constructor
	 * @param x Position in x dimension
	 * @param y Position in y dimension
	 */
	public Coord(int x, int y) { 
		this.x = x; 
		this.y = y; 
	} 
	
	/**
	 * Calculates the euclidean distance to another Coord
	 * @param otherCoord The coordinate to calculate the distance to
	 * @return The distance between the two coordinates
	 */
	public double distanceTo(Coord otherCoord) {
		return Math.sqrt(Math.pow(this.x - otherCoord.x, 2) + 
				Math.pow(this.y - otherCoord.y, 2));
	}
	
	/**
	 * Two Coords are equal if their x and y values are the same
	 */
	public boolean equals(Object obj) {
		Coord otherCoord = (Coord) obj;
		if (this.x == otherCoord.x && this.y == otherCoord.y) {
			return true;
		}
		else {
			return false;
		}
	}
	
	/**
	 * Hash value calculated from a Coord's x and y value
	 */
	public int hashCode() {
		int result = 43;
		result = 37 * result + x;
		result = 37 * result + y;
		return result;
	}
}


