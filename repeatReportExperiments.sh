./runModel.sh $1 False 100 100 0 "[(49,49)]" "" True data/100x100_vonNeumann_50-50_noWrap/ results/100x100_vonNeumann_50-50_noWrap/ netlogo_data/100x100_vonNeumann_50-50_noWrap/
./runModel.sh $1 False 100 100 1 "[(49,49)]" "" True data/100x100_Moore_50-50_noWrap/ results/100x100_Moore_50-50_noWrap/ netlogo_data/100x100_Moore_50-50_noWrap/
./runModel.sh $1 False 100 100 0 "[(0,49),(49,0)]" "" True data/100x100_vonNeumann_0-50_50-0_noWrap/ results/100x100_vonNeumann_0-50_50-0_noWrap/ netlogo_data/100x100_vonNeumann_0-50_50-0_noWrap/
./runModel.sh $1 True 100 100 0 "[(0,49),(49,0)]" "" True data/100x100_vonNeumann_0-50_50-0_wrap/ results/100x100_vonNeumann_0-50_50-0_wrap/ netlogo_data/100x100_vonNeumann_0-50_50-0_wrap/
./runModel.sh $1 False 100 100 0 10 "" True data/100x100_vonNeumann_rand10_noWrap/ results/100x100_vonNeumann_rand10_noWrap/ netlogo_data/100x100_vonNeumann_rand10_noWrap/
./runModel.sh $1 False 100 100 0 "[(49,49)]" "" False data/100x100_vonNeumann_50-50_noWrap_noRediscuss/ results/100x100_vonNeumann_50-50_noWrap_noRediscuss/ netlogo_data/100x100_vonNeumann_50-50_noWrap/
./runModel.sh $1 False 100 100 0 "[(1,2)]" "" True data/100x100_vonNeumann_1-2_noWrap_about-1-1/ results/100x100_vonNeumann_1-2_noWrap_about-1-1/ netlogo_data/100x100_vonNeumann_1-2_noWrap/ "(1,1)"
