# About
This project is a Java implementation of NetLogo's Rumour Mill model. A Java project is used to run the model and output CSV data of the results. A python script is then used to plot the results. These results are plotted against NetLogo data if provided.

# Requirements
To run the Python script you will need to have Matplotlib installed. Run, 'pip install matplotlib' or equivalent if you don't have it installed.

# How to Run
An Ant build file is provided with the Java project. The format of the command line arguments is as follows:
Usage: java -cp <classpath> <class> <iterations> <wrapAround> <xDimen> <yDimen> <neighbourhood> <[(rumourSourceX,rumourSourceY), ...] | initClique> <[(wallX,wallY), ...]> <rediscussRumour> <dataOutputFolder> <optional(rumourAboutX,rumourAboutY)>

The Python script can be run as follows:
./graphResults.py <data-dir> <result-dir> <optional netlogo-dir>

To simplify things, 'runModel.sh' is provided and will run the Java project and then run the Python script to graph the results.
./runModel.sh <iterations> <wrapAround> <xDimen> <yDimen> <neighbourhood> <[(rumourSourceX,rumourSourceY), ...] | initClique> <[(wallX, wallY), ...]> <rediscuss-rumour> <data-dir> <result-dir> <netlogo-dir> <optional (rumourAboutX, rumourAboutY)>

To make things even simpler to repeat the experiments in the project, a script ('repeatReportExperiments.sh'). This script only takes one argument, the amout of iterations to run the model for each time:
./repeatReportExperiments.sh <iterations>

If you need some examples of the format of the command line arguments, these can be easily found in the 'repeatReportExperiments' script.

NOTE: To run the model without NetLogo data, simply provide "" for the <netlogo-dir> argument.
