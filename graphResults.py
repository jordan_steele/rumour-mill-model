import sys
import os
import csv
import math
import matplotlib.pyplot as plt

def plot_line_graph(xLabel, yLabel, data, title, filename, output_dir):
    fig, ax = plt.subplots(1)
    for (label, xData, yData) in data:
        plt.plot(xData, yData, label=label)
    plt.xlabel(xLabel)
    plt.ylabel(yLabel)
    plt.grid(True)
    plt.title(title)
    plt.legend(loc='upper right')
    filename = os.path.abspath(os.path.join(output_dir, filename))
    plt.savefig(filename)

def plot_histogram(xLabel, yLabel, data, title, filename, output_dir):
    fig, ax = plt.subplots(1)
    text_box_ypos = 0.8
    for (label, values) in data:
        textstr = '%s\n$\mu=%.2f$\n$\mathrm{median}=%.2f$\n$\sigma=%.2f$'%(label, mean(values), median(values), standard_deviation(values))
        bbox_props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
        ax.text(0.76, text_box_ypos, textstr, transform=ax.transAxes, fontsize=14, verticalalignment='top', bbox=bbox_props)
        plt.hist(values, 30, alpha=0.5, label=label) 
        text_box_ypos -= 0.22
    plt.title(title)
    plt.xlabel(xLabel)
    plt.ylabel(yLabel)
    plt.legend(loc='upper right')
    filename = os.path.abspath(os.path.join(output_dir, filename))
    plt.savefig(filename)

def mean(data):
    return sum(data) / float(len(data))

def median(data):
    sorted_data = sorted(data)
    lenth_data = len(data)
    if not lenth_data % 2:
        return (sorted_data[lenth_data / 2] + sorted_data[lenth_data / 2 - 1]) / 2.0
    return sorted_data[lenth_data / 2]

def standard_deviation(data):
    mean_data = mean(data)
    return math.sqrt(sum([(val - mean_data)**2 for val in data])/(len(data) - 1))


def parse_csv(csv_filename, cols, cols_offset):
    data = ()
    for i in range(0, cols):
        data += ([],)

    with open(csv_filename, 'rb') as csvfile:
        csv_reader = csv.reader(csvfile, delimiter=',')
        next(csv_reader, None) #skip the header
        for row in csv_reader:
            for i in range(0, cols):
                data[i].append(float(row[i+cols_offset]))
    return data

def main():
    if len(sys.argv) < 3:
        sys.stderr.write("USAGE: %s <data-dir> <result-dir> <netlogo-dir>\n" % sys.argv[0])
        sys.exit()

    output_dir = os.path.abspath(sys.argv[2])
    
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    our_label = "Java"
    netlogo_label = "NetLogo"

    #parse data from csv to data for plotting
    rumour_spread_data = []
    succ_diff_data = []
    succ_ratio_data = []
    clique_percent = []
    times_heards_data = []
    first_times_heards_data = []

    #our data
    (ticks, cliques, delta_clique, successive_ratios) = parse_csv(sys.argv[1] + "runningData.csv", 4, 0)
    for clique in cliques:
        clique_percent.append(float(clique) / float(cliques[len(cliques) - 1]) * 100)

    (times_heards, first_times_heards, knowledge_of_rumours) = parse_csv(sys.argv[1] + "endData.csv", 3, 0)

    rumour_spread_data.append((our_label, ticks, clique_percent))
    succ_diff_data.append((our_label, ticks, delta_clique))
    succ_ratio_data.append((our_label, ticks, successive_ratios))
    times_heards_data.append((our_label, times_heards))
    first_times_heards_data.append((our_label, first_times_heards))

    #rumour mill data if provided
    if len(sys.argv) == 4:
        (ticks, clique_percent) = parse_csv(sys.argv[3] + "rumour_spread.csv", 2, 0)
        rumour_spread_data.append((netlogo_label, ticks, clique_percent))
        (ticks, succ_diff) = parse_csv(sys.argv[3] + "succ_diff.csv", 2, 0)
        succ_diff_data.append((netlogo_label, ticks, succ_diff))
        (ticks, successive_ratios) = parse_csv(sys.argv[3] + "succ_ratio.csv", 2, 0)
        succ_ratio_data.append((netlogo_label, ticks, successive_ratios))
        (times_heards, first_times_heards) = parse_csv(sys.argv[3] + "person_data.csv", 2, 5)
        times_heards_data.append((netlogo_label, times_heards))
        first_times_heards_data.append((netlogo_label, first_times_heards))

    #plot data
    plot_line_graph("Time", "Percent", rumour_spread_data, "Rumour Spread", "rumour_spread.pdf", output_dir)
    plot_line_graph("Time", "Difference", succ_diff_data, "Successive Differences", "successive_differences.pdf", output_dir)
    plot_line_graph("Time", "Ratio", succ_ratio_data, "Successive Ratios", "successive_ratios.pdf", output_dir)
    plot_histogram("Times Heard", "Distribution", times_heards_data, "Times Heard Distribution", "times_heard_distribution.pdf", output_dir)
    plot_histogram("First Time Heard (ticks)", "Distribution", first_times_heards_data, "First Time Heard Distribution", "first_time_heard_distribution.pdf", output_dir)
    plot_histogram("Knowledge of Rumour", "Distribution", [(our_label, knowledge_of_rumours)], "Knowledge of Rumour Distribution", "knowledge_of_rumour_distribution.pdf", output_dir)

if __name__ == "__main__":
    main()